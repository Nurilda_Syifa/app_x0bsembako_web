<?php
    $DB_NAME = "sembako";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $sql = "SELECT p.kode, p.nmproduk, p.ukuran, s.satuan, p.harga, p.foto
    FROM produk p, satuan s WHERE p.id_sat = s.id_sat";
    
    $result = mysqli_query($conn,$sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Harga Sembako</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body class="bg-light">
    <div class="container">
        <div class="row justify-content-center mt-5">
            <h4>Daftar Harga Sembako</h4>
            <table class="mt-3 table table-bordered">
                <tr>
                    <th>Kode</th>
                    <th>Produk</th>
                    <th>Netto</th>
                    <th>Satuan</th>
                    <th>Harga</th>
                    <th>Foto</th>
                    <th>Action</th>
                </tr>
                <?php 
                    while($sbk = mysqli_fetch_assoc($result)){
                ?>
                <tr>                 
                    <td><?php echo $sbk['kode']; ?></td>
                    <td><?php echo $sbk['nmproduk']; ?></td>
                    <td><?php echo $sbk['ukuran']; ?></td>
                    <td><?php echo $sbk['satuan']; ?></td>
                    <td><?php echo $sbk['harga']; ?></td>
                    <td><img src="images/<?php echo $sbk['foto']; ?>" style="width: 100px;" alt="Foto Sembako"></td>
                    <td><?php echo "<button><a href='proses-hapus.php?kode=".$sbk['kode']."'>Hapus</a></button>"; ?></td>
                </tr> 
                <?php } ?>
            </table>
        </div>
    </div>
</body>
</html>